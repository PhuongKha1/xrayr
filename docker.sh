clear
echo "   1. Cài đặt"
echo "   2. update config"
echo "   3. update xrayr"
echo "   4. Thêm node"
echo "   5. Log"
echo "   6. Restart"
echo "   7. Delete"
read -p "  Vui lòng chọn một số và nhấn Enter (Enter theo mặc định Cài đặt)  " num
[ -z "${num}" ] && num="1"

pre_install(){
 clear
	read -p "Nhập số web cần cài và nhấn Enter (Enter theo mặc định 1) " n
	 [ -z "${n}" ] && n="1"
    a=0
  while [ $a -lt $n ]
 do
  echo " web số $((a+1))"
  echo -e "[1] 4ghatde.com"
  echo -e "[2] 4g.giare.me"
  echo -e "[3] 4gsieure.net"
  echo -e "[4] thegioi4g.com"
  read -p "Web đang sử dụng:" api_host
  if [ "$api_host" == "1" ]; then
    api_host="quoctai.click"
    api_key="phamvanquoctai0209"
  elif [ "$api_host" == "2" ]; then
    api_host="4g.giare.me"
    api_key="phamvanquoctai0209"
    elif [ "$api_host" == "3" ]; then
    api_host="4gsieure.net"
    api_key="phamvanquoctai0209"
    elif [ "$api_host" == "4" ]; then
    api_host="thegioi4g.com"
    api_key="phamvanquoctai0209"
  else 
    api_host="quoctai.click"
    api_key="phamvanquoctai0209"
  fi


  # #link web 
  #   read -p " Nhập domain web (không cần https://):" api_host
  #   [ -z "${api_host}" ] && api_host=0
  echo "--------------------------------"
  if [ "$api_host" == "quoctai.click" ]; then
    host="4ghatde.com"
    else
    host="$api_host"
  fi
  echo "Bạn đã chọn https://${host}"
  echo "--------------------------------"


  #key web
  # read -p " Nhập key web :" api_key
  #   [ -z "${api_key}" ] && api_key=0
  
  #node id
    read -p " ID nút (Node_ID):" node_id
  [ -z "${node_id}" ] && node_id=0
  echo "-------------------------------"
  echo -e "Node_ID: ${node_id}"
  echo "-------------------------------"
  

#   #giới hạn thiết bị
# read -p "Giới hạn thiết bị :" DeviceLimit
#   [ -z "${DeviceLimit}" ] && DeviceLimit="0"
#   echo "-------------------------------"
#   echo "Thiết bị tối đa là: ${DeviceLimit}"
#   echo "-------------------------------"
  
  
#   #giới hạn tốc độ
# read -p "Giới hạn tốc độ :" SpeedLimit
#   [ -z "${SpeedLimit}" ] && SpeedLimit="0"
#   echo "-------------------------------"
#   echo "Tốc Độ tối đa là: ${SpeedLimit}"
#   echo "-------------------------------"
  
  #IP vps
 read -p "Nhập domain :" CertDomain
  [ -z "${CertDomain}" ] && CertDomain="0"
 echo "-------------------------------"
  echo "ip : ${CertDomain}"
 echo "-------------------------------"

 config
  a=$((a+1))
done
}


config(){
  cd /root/docker
  cat >dns.json <<EOF
{
    "servers": [
        "1.1.1.1",
        "8.8.8.8",
        "localhost"
    ],
    "tag": "dns_inbound"
}
EOF

cat >>config.yml<<EOF
  -
    PanelType: "V2board" # Panel type: SSpanel, V2board, PMpanel, Proxypanel, V2RaySocks
    ApiConfig:
      ApiHost: "https://$api_host"
      ApiKey: "$api_key"
      NodeID: $node_id
      NodeType: V2ray # Node type: V2ray, Shadowsocks, Trojan, Shadowsocks-Plugin
      Timeout: 30 # Timeout for the api request
      EnableVless: false # Enable Vless for V2ray Type
      EnableXTLS: false # Enable XTLS for V2ray and Trojan
      SpeedLimit: 0 # Mbps, Local settings will replace remote settings, 0 means disable
      DeviceLimit: 0 # Local settings will replace remote settings, 0 means disable
      RuleListPath: # /etc/XrayR/rulelist Path to local rulelist file
    ControllerConfig:
      ListenIP: 0.0.0.0 # IP address you want to listen
      SendIP: 0.0.0.0 # IP address you want to send pacakage
      UpdatePeriodic: 60 # Time to update the nodeinfo, how many sec.
      EnableDNS: false # Use custom DNS config, Please ensure that you set the dns.json well
      DNSType: AsIs # AsIs, UseIP, UseIPv4, UseIPv6, DNS strategy
      DisableUploadTraffic: false # Disable Upload Traffic to the panel
      DisableGetRule: false # Disable Get Rule from the panel
      DisableIVCheck: false # Disable the anti-reply protection for Shadowsocks
      DisableSniffing: True # Disable domain sniffing
      EnableProxyProtocol: false # Only works for WebSocket and TCP
      AutoSpeedLimitConfig:
        Limit: 0 # Warned speed. Set to 0 to disable AutoSpeedLimit (mbps)
        WarnTimes: 0 # After (WarnTimes) consecutive warnings, the user will be limited. Set to 0 to punish overspeed user immediately.
        LimitSpeed: 0 # The speedlimit of a limited user (unit: mbps)
        LimitDuration: 0 # How many minutes will the limiting last (unit: minute)
      GlobalDeviceLimitConfig:
        Limit: 0 # The global device limit of a user, 0 means disable
        RedisAddr: 127.0.0.1:6379 # The redis server address
        RedisPassword: YOUR PASSWORD # Redis password
        RedisDB: 0 # Redis DB
        Timeout: 5 # Timeout for redis request
        Expiry: 60 # Expiry time (second)
      EnableFallback: false # Only support for Trojan and Vless
      FallBackConfigs:  # Support multiple fallbacks
        -
          SNI: # TLS SNI(Server Name Indication), Empty for any
          Alpn: # Alpn, Empty for any
          Path: # HTTP PATH, Empty for any
          Dest: 80 # Required, Destination of fallback, check https://xtls.github.io/config/features/fallback.html for details.
          ProxyProtocolVer: 0 # Send PROXY protocol version, 0 for dsable
      CertConfig:
        CertMode: dns # Option about how to get certificate: none, file, http, dns. Choose "none" will forcedly disable the tls config.
        CertDomain: "$CertDomain" # Domain to cert
        CertFile: /root/docker/4ghatde.crt # Provided if the CertMode is file
        KeyFile: /root/docker/4ghatde.key
        Provider: cloudflare # DNS cert provider, Get the full support list here: https://go-acme.github.io/lego/dns/
        Email: test@me.com
        DNSEnv: # DNS ENV option used by DNS provider
          CLOUDFLARE_EMAIL: qtai020901@gmail.com
          CLOUDFLARE_API_KEY: 0ba52dd8eec192ed1c33a4a413c96c2da1ea4
EOF

#   sed -i "s|ApiHost: \"https://domain.com\"|ApiHost: \"${api_host}\"|" ./config.yml
 # sed -i "s|ApiKey:.*|ApiKey: \"${ApiKey}\"|" 
#   sed -i "s|NodeID: 41|NodeID: ${node_id}|" ./config.yml
#   sed -i "s|DeviceLimit: 0|DeviceLimit: ${DeviceLimit}|" ./config.yml
#   sed -i "s|SpeedLimit: 0|SpeedLimit: ${SpeedLimit}|" ./config.yml
#   sed -i "s|CertDomain:\"node1.test.com\"|CertDomain: \"${CertDomain}\"|" ./config.yml
 }



install_docker(){
  curl -fsSL https://get.docker.com | bash -s docker
curl -L "https://github.com/docker/compose/releases/download/1.26.1/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose
}

case "${num}" in
1) 
clear
cd /root
mkdir docker
cd /root/docker
  cat >config.yml <<EOF
Log:
  Level: none # Log level: none, error, warning, info, debug 
  AccessPath: # /etc/XrayR/access.Log
  ErrorPath: # /etc/XrayR/error.log
DnsConfigPath: # /etc/XrayR/dns.json # Path to dns config, check https://xtls.github.io/config/dns.html for help
RouteConfigPath: # /etc/XrayR/route.json # Path to route config, check https://xtls.github.io/config/routing.html for help
OutboundConfigPath: # /etc/XrayR/custom_outbound.json # Path to custom outbound config, check https://xtls.github.io/config/outbound.html for help
ConnectionConfig:
  Handshake: 4 # Handshake time limit, Second
  ConnIdle: 30 # Connection idle time limit, Second
  UplinkOnly: 2 # Time limit when the connection downstream is closed, Second
  DownlinkOnly: 4 # Time limit when the connection is closed after the uplink is closed, Second
  BufferSize: 64 # The internal cache size of each connection, kB  
Nodes:
EOF
 cat >docker-compose.yml <<EOF
version: '3'
services: 
  xrayr: 
    image: ghcr.io/xrayr-project/xrayr:latest
    volumes:
        - ./config.yml:/etc/XrayR/config.yml # thư mục cấu hình bản đồ
    restart: always
    network_mode: host
EOF
pre_install
install_docker
docker-compose up -d
;;
2)
  cd /root/docker
  echo "đóng dịch vụ hiện tại"
  docker-compose down
  cat >config.yml <<EOF
Log:
  Level: none # Log level: none, error, warning, info, debug 
  AccessPath: # /etc/XrayR/access.Log
  ErrorPath: # /etc/XrayR/error.log
DnsConfigPath: # /etc/XrayR/dns.json # Path to dns config, check https://xtls.github.io/config/dns.html for help
RouteConfigPath: # /etc/XrayR/route.json # Path to route config, check https://xtls.github.io/config/routing.html for help
OutboundConfigPath: # /etc/XrayR/custom_outbound.json # Path to custom outbound config, check https://xtls.github.io/config/outbound.html for help
ConnectionConfig:
  Handshake: 4 # Handshake time limit, Second
  ConnIdle: 30 # Connection idle time limit, Second
  UplinkOnly: 2 # Time limit when the connection downstream is closed, Second
  DownlinkOnly: 4 # Time limit when the connection is closed after the uplink is closed, Second
  BufferSize: 64 # The internal cache size of each connection, kB 
Nodes:
EOF
pre_install

  echo "Bắt đầu chạy dịch vụ DOKCER"
  docker-compose up -d
;;
3)
cd /root/docker
  echo "Tải hình ảnh DOCKER"
  docker-compose pull
  echo "Bắt đầu chạy dịch vụ DOCKER"
  docker-compose up -d

;;
5)cd /root/docker
  echo "100 dòng nhật ký chạy sẽ được hiển thị"
  docker-compose logs --tail 100
;;
6)cd /root/docker
  docker-compose down
  docker-compose up -d
  echo "Khởi động lại thành công!"
;;
7)cd /root/docker
docker-compose down
  cd ~
  rm -Rf ${cur_dir}
  echo "đã xóa thành công!"
;;
4)cd /root/docker
docker-compose down
pre_install
docker-compose up -d
echo "đã thêm node!"
;;
esac
