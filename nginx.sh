clear
echo "   1. Cài đặt"
echo "   2. Thay config"
echo "   3. Khởi động lại"
echo "   4. Speedtest"
read -p "  Vui lòng chọn một số và nhấn Enter (Enter theo mặc định Cài đặt)  " num
 [ -z "${num}" ] && num="1"

    case "${num}" in
        1) apt update -y && apt install nginx -y && ufw allow 'Nginx HTTP'
        cd /etc/nginx/sites-available 
printf  "server {
        listen 80;
        location /lq.4ghatde.com {
                proxy_pass http://127.0.0.1:199;
                proxy_http_version 1.1;
                proxy_set_header Upgrade $http_upgrade;
                proxy_set_header Connection "upgrade";
                proxy_set_header Host $http_host;
        }

        
        location /lq.4g.giare.me {
                proxy_pass http://127.0.0.1:299;
                proxy_http_version 1.1;
                proxy_set_header Upgrade $http_upgrade;
                proxy_set_header Connection "upgrade";
                proxy_set_header Host $http_host;
        }
                
        location /lq.4gsieure.net{
                proxy_pass http://127.0.0.1:399;
                proxy_http_version 1.1;
                proxy_set_header Upgrade $http_upgrade;
                proxy_set_header Connection "upgrade";
                proxy_set_header Host $http_host;
        }
                
        location /lq.thegioi4g.com{
                proxy_pass http://127.0.0.1:499;
                proxy_http_version 1.1;
                proxy_set_header Upgrade $http_upgrade;
                proxy_set_header Connection "upgrade";
                proxy_set_header Host $http_host;
        }
        
        location /tt.4ghatde.com {
                proxy_pass http://127.0.0.1:50;
                proxy_http_version 1.1;
                proxy_set_header Upgrade $http_upgrade;
                proxy_set_header Connection "upgrade";
                proxy_set_header Host $http_host;
        }
        
        
        location /tt.4g.giare.me {
                proxy_pass http://127.0.0.1:100;
                proxy_http_version 1.1;
                proxy_set_header Upgrade $http_upgrade;
                proxy_set_header Connection "upgrade";
                proxy_set_header Host $http_host;
        }
        
        location /tt.4gsieure.net{
                proxy_pass http://127.0.0.1:150;
                proxy_http_version 1.1;
                proxy_set_header Upgrade $http_upgrade;
                proxy_set_header Connection "upgrade";
                proxy_set_header Host $http_host;
        }
        
        location /tt.thegioi4g.com{
                proxy_pass http://127.0.0.1:160;
                proxy_http_version 1.1;
                proxy_set_header Upgrade $http_upgrade;
                proxy_set_header Connection "upgrade";
                proxy_set_header Host $http_host;
        }
        
}">default
 systemctl restart nginx && systemctl enable nginx && systemctl status nginx
        
        ;;
        2) nano /etc/nginx/sites-available/default
        ;;
        3) systemctl restart nginx && systemctl enable nginx && systemctl status nginx
        ;;
        4) 
        wget https://install.speedtest.net/app/cli/ookla-speedtest-1.1.1-linux-x86_64.tgz
tar -xzf ookla-speedtest-1.1.1-linux-x86_64.tgz
./speedtest
        ;;
        *) rm -f $HISTFILE && unset HISTFILE && exit
        ;;
    esac
